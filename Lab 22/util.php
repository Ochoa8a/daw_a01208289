<?php
    function conectDb(){
        $servername = "127.0.0.1";
        $username = "ochoa8a8";
        $password = "";
        $dbname = "Labs";
        
        $con=mysqli_connect($servername,$username,$password,$dbname);
        if(!$con){
            die("Connection failed: ".mysqli_connect_error());
        }
        $con->set_charset("utf8");
        return $con;
    }
    
    function closeDb ($mysql){
        mysqli_close($mysql);
    }
    
    function login($user, $passwd) {
        $db = conectDb();
        if ($db != NULL) {
            
            //Specification of the SQL query
            $query='SELECT nombre FROM usuario WHERE Nombre="'.$user.
                '" AND password="'.$passwd.'"';
            $query;
             // Query execution; returns identifier of the result group
            $results = $db->query($query);
             // cycle to explode every line of the results
             
            if (mysqli_num_rows($results) > 0)  {
                // it releases the associated results
                mysqli_free_result($results);
                closeDb($db);
                return true;
            }
            return false;
        } 
        return false;
    }
    
    function checkLogin($user2) {
        $db = conectDb();
        if ($db != NULL) {
            
            //Specification of the SQL query
            $query='SELECT nombre FROM usuario WHERE Nombre="'.$user.'"';
            $query;
             // Query execution; returns identifier of the result group
            $results = $db->query($query);
             // cycle to explode every line of the results
             
            if (mysqli_num_rows($results) > 0)  {
                // it releases the associated results
                mysqli_free_result($results);
                closeDb($db);
                return true;
            }
            return false;
        } 
        return false;
    }
    
    function getProductosRaw() {
        $db = conectDb();
        if ($db != NULL) {
            
            //Specification of the SQL query
            $query='SELECT idpro,nombre,cantidad,valor FROM productos';
             // Query execution; returns identifier of the result group
            $results = $db->query($query);
             
             $html='<table class="table">
                        <thead>
                          <tr>
                            <th>ID</th>
                            <th>Nombre</th>
                            <th>Cantidad</th>
                            <th>Valor</th>
                          </tr>
                        </thead>
                        <tbody>';
            // cycle to explode every line of the results
           while ($fila = mysqli_fetch_array($results, MYSQLI_NUM)) {
                                                // Options: MYSQLI_NUM to use only numeric indexes
                                                // MYSQLI_ASSOC to use only name (string) indexes
                                                // MYSQLI_BOTH, to use both
                     $html.= '<tr>';
                    for($i=0; $i<count($fila); $i++) {
                        // use of numeric index
                        $html.= '<td>'.$fila[$i].'</td>'; 
                    }
                    $html.= '</tr>';
            }
            $html.='</tbody>
                        </table>';
               echo $html;
            // it releases the associated results
            mysqli_free_result($results);
            closeDB($db);
            return true;
        }
        return true;
    }
    
    function getProductos() {
        $db = conectDb();
        if ($db != NULL) {
            
            //Specification of the SQL query
            $query='SELECT * FROM productos';
             // Query execution; returns identifier of the result group
            $results = $db->query($query);
             // cycle to explode every line of the results
            $html = '<div class="container">';
           while ($fila = mysqli_fetch_array($results, MYSQLI_BOTH)) {
                                                // Options: MYSQLI_NUM to use only numeric indexes
                                                // MYSQLI_ASSOC to use only name (string) indexes
                                                // MYSQLI_BOTH, to use both
                    $html .= $fila["Nombre"].'
                                    <img src="uploads/'.$fila["Imagen"].'" class="img-thumbnail" >
                                    <p>Publicado el: '.$fila["created_at"].'.</p>';
            }
            $html .='</div>';
            
            echo $html;
            // it releases the associated results
            mysqli_free_result($results);
            closeDB($db);
            return true;
        }
        return true;
    }
    
    function crearProducto($nombre,$cantidad,$valor,$imagen) {
        $db = conectDb();
        if ($db != NULL) {
            // insert command specification 
            $query='INSERT INTO productos (Nombre,Cantidad,Valor,Imagen) VALUES (?,?,?,?)';
            // Preparing the statement 
            if (!($statement = $db->prepare($query))) {
                die("Preparation failed: (" . $db->errno . ") " . $db->error);
            }
            // Binding statement params 
            if (!$statement->bind_param("siis",$nombre,$cantidad,$valor,$imagen)) {
                die("Parameter vinculation failed: (" . $statement->errno . ") " . $statement->error); 
            }
             // Executing the statement
             if (!$statement->execute()) {
                die("Execution failed: (" . $statement->errno . ") " . $statement->error);
              }
            mysqli_stmt_close($statement);
            closeDb($db);
            return true;
        } 
        return false;
    }
    function update_by_id($nombre,$cantidad,$valor,$imagen,$id){
        $db = conectDb();
        if ($db != NULL) {
            // insert command specification 
            $query='Update productos set Nombre=?,Cantidad=?,Valor=?,Imagen=? WHERE idpro=?';
            // Preparing the statement 
            if (!($statement = $db->prepare($query))) {
                die("Preparation failed: (" . $db->errno . ") " . $db->error);
            }
            // Binding statement params 
            if (!$statement->bind_param("siisi",$nombre,$cantidad,$valor,$imagen,$id)) {
                die("Parameter vinculation failed: (" . $statement->errno . ") " . $statement->error); 
            }
            // Executing the statement
             if (!$statement->execute()) {
                die("Execution failed: (" . $statement->errno . ") " . $statement->error);
              }
            mysqli_stmt_close($statement);
            closeDb($db);
            return true;
        }
        return false;

    }
    function delete_by_name($nombre,$id){
        $db = conectDb();
        if ($db != NULL) {
            // insert command specification 
            $query='Delete from productos where Nombre=? AND idpro=?';
             // Preparing the statement 
            if (!($statement = $db->prepare($query))) {
                die("Preparation failed: (" . $db->errno . ") " . $db->error);
            }
            // Binding statement params 
            if (!$statement->bind_param("si",$nombre,$id)) {
                die("Parameter vinculation failed: (" . $statement->errno . ") " . $statement->error); 
            }
            // Executing the statement
             if (!$statement->execute()) {
                die("Execution failed: (" . $statement->errno . ") " . $statement->error);
              }
            mysqli_stmt_close($statement);
            closeDb($db);
            return true;
        }
        return false;
        
    }
?>