<?php
    session_start();
    require_once("util.php");
    include("extra/_header.html");
    if(isset($_SESSION["usrname"])){
            include("extra/_Success.html");
    }
    else if(login(htmlspecialchars($_POST["usrname"]), htmlspecialchars($_POST["psw"]))){
        unset($_SESSION["error"]);
        $_SESSION["usrname"]=$_POST["usrname"];
        $_SESSION["producto"]=" ";
        include("extra/_Success.html");
    }
    else{
        $_SESSION["error"] = "Usuario y/o contraseña incorrectos";
        header("location: index.php");
    }
    include("extra/_footer.html");
?>