<?php
    require_once("util.php");
    $nombre=$_POST["ENombre"];
    $cantidad=$_POST["ECantidad"];
    $valor=$_POST["EValor"];
    $imagen=$_POST["EImagen"];
    $id=$_POST["EId"];
    
    if(strlen($nombre)>0 && strlen($cantidad)>0 && strlen($valor)>0 && strlen($imagen)>0 && strlen($id)>0){
        if(is_numeric($cantidad) && is_numeric($valor) && is_numeric($id)){
            if(update_by_id($nombre,$cantidad,$valor,$imagen,$id)){
                header ("location:login.php");
            }
            else 
                echo "<script type='text/javascript'>alert('Errores al crear');</script>";
        }else 
            echo "<script type='text/javascript'>alert('Errores en cantidad o valor');</script>";
    }else
        echo "<script type='text/javascript'>alert('Errores en la forma');</script>";
?>