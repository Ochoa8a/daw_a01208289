<?php
    require_once("util.php");
    $nombre=$_POST["Nombre"];
    $cantidad=$_POST["Cantidad"];
    $valor=$_POST["Valor"];
    $imagen=$_POST["Imagen"];
    
    if(strlen($nombre)>0 && strlen($cantidad)>0 && strlen($valor)>0 && strlen($imagen)>0){
        if(is_numeric($cantidad) && is_numeric($valor)){
            if(crearProducto($nombre,$cantidad,$valor,$imagen)){
                header ("location:login.php");
            }
            else 
                echo "<script type='text/javascript'>alert('Errores al crear');</script>";
        }else 
            echo "<script type='text/javascript'>alert('Errores en cantidad o valor');</script>";
    }else
        echo "<script type='text/javascript'>alert('Errores en la forma');</script>";
?>