<?php
    require_once("util.php");
    $nombre=$_POST["ELNombre"];
    $id=$_POST["ELId"];
    
    if(strlen($nombre)>0 && strlen($id)>0){
        if(is_numeric($id)){
            if(delete_by_name($nombre,$id)){
                header ("location:login.php");
            }
            else 
                echo "<script type='text/javascript'>alert('Errores al crear');</script>";
        }else 
            echo "<script type='text/javascript'>alert('Errores en cantidad o valor');</script>";
    }else
        echo "<script type='text/javascript'>alert('Errores en la forma');</script>";
?>