<?php include("extra/_header.html"); ?>
        
        <section class="hero">
            <div class="wrapper">
                <header><h1>Laboratorio 9</h1></header>
            </div>
        </section>
        <section class="main">
            <form action= "arreglo.php" method="get" class="wrapper">
                <header><h3>Numeros</h3></header>
                <?php 
                    for($i=1;$i<11;$i++){
                        echo ("<label for='n$i'>Número $i</label>"."<input type='number' name='n$i' id='n$i' value=".rand(1,100)."><br>");
                    }//Crea campos de manera epica fantastica y les da un valor aleatorio entre 1 y 100
                ?>
                <label for="probl">Problema a resolver</label>
                <select name="probl">
                    <option value="probl1">Promedio</option>
                    <option value="probl2">Mediana</option>
                    <option value="probl3">Arreglo</option>
                </select>
                <input type="submit">
            </form>
            <form action="potencias.php" method="get" class="wrapper">
                <header><h3>Cuadrados y cubos</h3></header>
                <label for="pot">Números a tabular</label>
                <input type="number" value=0 name="pot" id="pot">
                <br>
                <input type="submit">
            </form>
            <form action="primos.php" method="get" class="wrapper">
                <header><h3>Calculadora de primos</h3></header>
                <label for="inv">Numeros primos hasta:</label>
                <input type="number" value=0 name="inv" id="inv">
                <br>
                <input type="submit">
            </form>
            <!-- ejercicio 5-->
            <article class="wrapper">
                <header><h3>Preguntas</h3></header>
                <ol>
                    <li>¿Qué hace la función phpinfo()? Describe y discute 3 datos que llamen tu atención.<br>-Muestra gran cantidad de información
                    sobre el estado actual de PHP. Incluye información sobre las opciones de compilación y extensiones de PHP, versión de PHP,
                    información del servidor y entorno (si se compiló como módulo), entorno PHP, versión del OS, rutas, valor de las opciones de 
                    configuración locales y generales, cabeceras HTTP y licencia de PHP.<ul>
                        <li>INFO_GENERAL</li>
                        <li>INFO_CONFIGURATION</li>
                        <li>INFO_ENVIRONMENT</li>
                    </ul></li>
                    <li>¿Qué cambios tendrías que hacer en la configuración del servidor para que pudiera ser apto en un ambiente de producción?<br>
                    -Cambiar el archivo php.ini-development por php.ini-production, para que inicie con la configuración de ambiente producción.</li>
                    <li>¿Cómo es que si el código está en un archivo con código html que se despliega del lado del cliente, se ejecuta del lado del
                    servidor? Explica.<br>-Se ejecuta un archivo php que devuelve un archivo html para que lo visualice el cliente.</li>
                </ol>
                </article>
        </section>
<?php include("extra/_footer.html"); ?> 