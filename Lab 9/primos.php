<?php include("extra/_header.html"); ?>
       
        <section class="hero">
            <div class="wrapper"><h2>Calculadora de primos</h2></div>
        </section>
        <?php
            echo '<section class="main wrapper">';
            $n=$_GET["inv"];
            
            $array = array(); $upperLimit = sqrt($n); $primes = array();
            for ($i = 0;$i < $n; $i++) {
                array_push($array,true);
            }
        	for ($i = 2; $i <= $upperLimit; $i++) {
                if ($array[$i]) {
                    for ($j = $i * $i; $j < $n; $j += $i) {
                        $array[$j] = false;
                    }
                }
            }
            for ($i = 2; $i < $n; $i++) {
                if($array[$i]) {
                    array_push($primes,$i);
                }
            }
            echo "Numeros primos hasta ".$n."<br>";
            $count=1;
            foreach($primes as $primo){
                echo $primo.", ";
                if($count==0)echo "<br>";
                $count=$count++/10;
            }
        ?>
<?php include("extra/_footer.html"); ?> 