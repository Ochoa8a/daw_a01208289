<?php include("extra/_header.html"); 
    function prom($arr){
        $suma=0;
        foreach($arr as $value){
            $suma+=$value;
        }
        return $suma/count($arr);
    }

?>
        <section class="hero">
            <div class="wrapper"><h2>Problemas con arreglos</h2></div>
        </section>
        <?php
            $arr=array();
            for($i=0;$i<10;$i++){
                $j=$i+1;
                array_push($arr,$_GET["n"."$j"]);
            }
            switch($_GET["probl"]){
                case "probl1":
                    echo '<section class="main"><div class="wrapper"><h2>Problema 1</h2>';
                    echo "El promedio del arreglo es " . prom($arr)."</div></section>";
                    break;
                case "probl2":
                    echo '<section class="main"><div class="wrapper"><h2>Problema 2</h2>';
                    sort($arr);
                    echo "La mediana es: ".($arr[4]+$arr[5])/2 . "</div></section>";
                    break;
                case "probl3":
                    echo '<section class="main"><div class="wrapper"><h2>Problema 3</h2>';
                    $suma=0;
                    for($i=0;$i<10;$i++){
                        $suma+=$arr[$i];
                        echo $arr[$i]." ";
                    }
                    echo "<br>";
                    sort($arr);
                    echo "<ol><li>El promedio es: ".(($suma)/10)."</li><br>";
                    echo "<li>La mediana es: ".(($arr[4]+$arr[5])/2)."</li><br>";
                    echo "<li>Sort ascending: ";
                    for($i=0;$i<$n;$i++){
                        echo "$arr[$i] ";
                    }
                    echo "</li><br>";
                    echo "<li>Sort descending: ";
                    rsort($arr);
                    for($i=0;$i<$n;$i++){
                        echo "$arr[$i] ";
                    }
                    echo "</li><br></div></section>";
                    break;
            }
        ?>
<?php include("extra/_footer.html"); ?> 