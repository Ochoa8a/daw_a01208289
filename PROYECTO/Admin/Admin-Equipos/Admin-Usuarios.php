<!doctype html>
<html lang="en">
  <head>
    
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="maincolors.css">
<link href="https://fonts.googleapis.com/css?family=Montserrat|Open+Sans" rel="stylesheet"> 
    <title>Admin-Usuarios</title>
  </head>
  <body>
<!-- SESION ACTIVA PHP -->





<!-- HEADER -->
   <header >
    <div class="container-fluid mitad1" >
      <img src="Images/img-nav2.png" class="img-responsive">
    </div>

</header>


<!--
      Documento 


-->
<div class="header_bg img-responsive">
  <div class="container">
    
    <div class="row header ">
      
    <div class="logo navbar-left">
      <h1>
        GESTIÓN DE USUARIOS
      </h1>
    </div>
    <div class="clearfix"></div>
    
  </div>
 
</div>
<nav class="navbar navbar-default  menu container">
  <div class="container navi">
    <div class="navbar-header">
      
    </div>
    <ul class="nav navbar-nav ">
      <li ><a href="Admin.php">EQUIPOS</a></li>
      <li class="activa" ><a href="Admin-Usuarios.php">USUARIOS</a></li>
      <li><a href="Admin-Control.php">ARCHIVOS</a></li>
      <li><a href="Admin-Eventos.php">CONTROL</a></li>
    </ul>
    <a href="cierre_sesion.php"><img src="Images/logout.png" class="img-responsive salir"></a>
  </div>
</nav>

</div>
<div class="clearfix"></div>



<!---***************************************************-->

<section class="main row">

    <article class="container">
<!-- Filtro de Usuarios-->
<div class="form-group busqueda">
  <div class="form-inline">
    <label class="sr-only">Ordenar Por</label>
    <select class="form-control" id="ordenarU" name="ordenarU">
      <option>Ordenar Por</option>
      <option>ID</option>
      <option>Nombre</option>
      <option>Equipo</option>
      <option>Rol</option>
    </select>
          <button class=" boton-usuario btn btn-default col-sm-12 col-md-5 " id="edi">Editar Usuario</button>
           <button class=" boton-usuario btn btn-primary  col-sm-12 col-md-5" id="agr">Agregar Usuario</button>
</div>



  </div>


<!-- AGREGAR EDITAR -->

<div class=" container" id="agregaT">
  <table>
  <caption>AGREGAR INTEGRANTE</caption>
  <thead>
    <tr>
     
      <th scope="col">NOMBRE</th>
      <th scope="col">APELLIDO PATERNO </th>
      <th scope="col"> APELLIDO MATERNO</th>
      <th scope="col">DIRECCION</th>
      <th scope="col">TELEFONO</th>
      </tr>
  </thead>
  <tbody>
    <tr>
     
      <td data-label="Nombre"><input type="text" name="nombre" value="Adrian"></td>
      <td data-label="ApellidoP"><input type="text" name="nombre" value="Valdezz"></td>
      <td data-label="ApellidoM"><input type="text" name="nombre" value="Juarez"></td>
      <td data-label="Direccion"><input type="text" name="nombre" value="Robles 28"></td>
      <td data-label="telefono"><input type="text" name="nombre" value="44332837"></td>
    
      
    </tr>
    <tr>
      <td   colspan="5"><a href="#"><button class="btn btn-primary" id="oka">Agregar</button></a></td>
    </tr>
  </tbody>
</table>
</div>
<div class=" container" id="actualizaT">
  <table>
  <caption>EDITAR INTEGRANTE</caption>
  <thead>
    <tr>
     
      <th scope="col">NOMBRE</th>
      <th scope="col">APELLIDO PATERNO </th>
      <th scope="col"> APELLIDO MATERNO</th>
      <th scope="col">DIRECCION</th>
      <th scope="col">TELEFONO</th>
      </tr>
  </thead>
  <tbody>
    <tr>
     
      <td data-label="Nombre"><input type="text" name="nombre" value="Adrian"></td>
      <td data-label="ApellidoP"><input type="text" name="nombre" value="Valdezz"></td>
      <td data-label="ApellidoM"><input type="text" name="nombre" value="Juarez"></td>
      <td data-label="Direccion"><input type="text" name="nombre" value="Robles 28"></td>
      <td data-label="telefono"><input type="text" name="nombre" value="44332837"></td>
    
      
    </tr>
    <tr>
      <td   colspan="5"><a href="#"><button class="btn btn-default" id="oke">Actualizar</button></a></td>
    </tr>
  </tbody>
</table>
</div>


<!--FIN AGREGAR ELIMINAR-->

<!-- INTEGRANTES -->
<table>
  <caption>Usuarios</caption>
  <thead>
    <tr>
      <th scope="col">ID</th>
      <th scope="col">Nombre</th>
      <th scope="col">Apellido</th>
      <th scope="col">Equipo</th>
      <th scope="col">Rol</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td data-label="IdU">0001</td>
      <td data-label="Nombre">Andres</td>
      <td data-label="Apellido">Gonzalez</td>
      <td data-label="Equipo">1</td>
      <td data-label="Rol">Lider</td>
    </tr>
    <tr>
      <td scope="row" data-label="IdU">0002</td>
      <td data-label="Nombre">Ignacio</td>
      <td data-label="Apellido">Perez</td>
      <td data-label="Equipo">5</td>
      <td data-label="Rol">Seguridad</td>
    </tr>
    <tr>
      <td scope="row" data-label="IdU">0003</td>
      <td data-label="Nombre">Rodrigo</td>
      <td data-label="Apellido">Gil</td>
      <td data-label="Equipo">3</td>
      <td data-label="Rol">Tallerista</td>
    </tr>
    <tr>
      <td scope="row" data-label="IdU">0004</td>
      <td data-label="Nombre">Gonzalo</td>
      <td data-label="Apellido">Garcia</td>
      <td data-label="Equipo">8</td>
      <td data-label="Rol">Tallerista</td>
    </tr>
    <tr>
      <td data-label="IdU">0005</td>
      <td data-label="Nombre">Luis</td>
      <td data-label="Apellido">Grijalva</td>
      <td data-label="Equipo">1</td>
      <td data-label="Rol">Seguridad</td>
    </tr>
    <tr>
      <td scope="row" data-label="IdU">0006</td>
      <td data-label="Nombre">Rosalio</td>
      <td data-label="Apellido">Herrera</td>
      <td data-label="Equipo">4</td>
      <td data-label="Rol">Lider</td>
    </tr>
    <tr>
      <td scope="row" data-label="IdU">0007</td>
      <td data-label="Nombre">Mateo</td>
      <td data-label="Apellido">Benavides</td>
      <td data-label="Equipo">9</td>
      <td data-label="Rol">Seguridad</td>
    </tr>
    <tr>
      <td scope="row" data-label="IdU">0008</td>
      <td data-label="Nombre">Ramiro</td>
      <td data-label="Apellido">Gonzales</td>
      <td data-label="Equipo">1</td>
      <td data-label="Rol">Tallerista</td>
    </tr>
  </tbody>
</table>


</article>


    <!--LOGO DEL COLEGIO -->

    <aside class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
     
       

    </aside>


</section>




<footer >
 <div class=" container-fluid borde">
   
 </div>
</footer>














    <!-- Scripts -->



      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script
        src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous"></script>


                        <script >
  
  $(document).ready(function(){
/* AGREGAR */
    $("#agregaT").hide();
     $("#actualizaT").hide();
    

    $("#agr").click(function(){

      $("#actualizaT").hide();
      
      $("#agregaT").show();


    });


    $("#oka").click(function(){

      $("#agregaT").hide();


    });




 
/* ELIMINAR */

   

    $("#edi").click(function(){
      $("#agregaT").hide();
     
      $("#actualizaT").show();


    });


    $("#oke").click(function(){

      $("#actualizaT").hide();


    });

  });

</script>
  </body>
</html>