<?php
    function conectDb(){
        $servername = "127.0.0.1";
        $username = "ochoa8a8";
        $password = "";
        $dbname = "Labs";
        
        $con=mysqli_connect($servername,$username,$password,$dbname);
        if(!$con){
            die("Connection failed: ".mysqli_connect_error());
        }
        $con->set_charset("utf8");
        return $con;
    }
    
    function closeDb ($mysql){
        mysqli_close($mysql);
    }
    
    function REGISTRAR_RETIRO_CAJERO($cuenta,$monto_retiro){
        $db=conectDb();
        
        if (!$db->query("DROP PROCEDURE IF EXISTS retiro_cajero") ||
            !$db->query("CREATE PROCEDURE retiro_cajero(IN cuenta VARCHAR, monto_retiro DECIMAL) 
            BEGIN TRANSACTION RETIRO
                UPDATE Clientes_Banca SET Saldo=Saldo-monto_retiro WHERE NoCuenta=cuenta;
                INSERT INTO Movimientos VALUES (cuenta,'001',monto_retiro);
                
                IF @@ERROR = 0
                    COMMIT TRANSACTION RETIRO;
                ELSE
                BEGIN
                    PRINT 'Error en el retiro';
                    ROLLBACK TRANSACTION RETIRO;
                END;
            END;")) {
            echo "Falló la creación del procedimiento almacenado: (" . $db->errno . ") " . $db->error;
        }
        
        if (!$db->query("CALL retiro_cajero(".$cuenta.",".$monto_retiro.")")) {
            echo "Falló CALL: (" . $db->errno . ") " . $db->error;
        }
    }
    
    function DEPOSITO_VENTANILLA($cuenta,$monto_depositar){
        $db=conectDb();
        
        if (!$db->query("DROP PROCEDURE IF EXISTS depositar") ||
            !$db->query("CREATE PROCEDURE depositar(IN cuenta VARCHAR, monto_deposito DECIMAL) 
            BEGIN TRANSACTION DEPOSITO
                UPDATE Clientes_Banca SET Saldo=Saldo+monto_deposito WHERE NoCuenta=cuenta;
                INSERT INTO Movimientos VALUES (cuenta,'002',monto_deposito);
                
                IF @@ERROR = 0
                    COMMIT TRANSACTION DEPOSITO;
                ELSE
                BEGIN
                    PRINT 'Error al depositar';
                    ROLLBACK TRANSACTION DEPOSITO;
                END;
            END;")) {
            echo "Falló la creación del procedimiento almacenado: (" . $db->errno . ") " . $db->error;
        }
        
        if (!$db->query("CALL depositar(".$cuenta.",".$monto_depositar.")")) {
            echo "Falló CALL: (" . $db->errno . ") " . $db->error;
        }
    }
?>