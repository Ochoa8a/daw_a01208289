IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'Entregan')
DROP TABLE Entregan
IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'Materiales')
DROP TABLE Materiales
IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'Proveedores')
DROP TABLE Proveedores
IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'Proyectos')
DROP TABLE Proyectos
CREATE TABLE Materiales 
( 
  Clave numeric(5) not null, 
  Descripcion varchar(50), 
  Costo numeric (8,2) 
)
ALTER TABLE Materiales add constraint llaveMateriales PRIMARY KEY (Clave)
CREATE TABLE Proveedores 
( 
  RFC char(13) not null, 
  RazonSocial varchar(50) 
)
ALTER TABLE Proveedores add constraint llaveProveedores PRIMARY KEY (RFC)
CREATE TABLE Proyectos 
( 
  Numero numeric(5) not null, 
  Denominacion varchar(50) 
)
ALTER TABLE Proyectos add constraint llaveProyectos PRIMARY KEY (Numero)
CREATE TABLE Entregan 
( 
  Clave numeric(5) not null, 
  RFC char(13) not null, 
  Numero numeric(5) not null, 
  Fecha DateTime not null, 
  Cantidad numeric (8,2) 
)
ALTER TABLE Entregan add constraint llaveEntregan PRIMARY KEY (Clave,RFC,Numero,Fecha)
ALTER TABLE entregan add constraint FKClave FOREIGN KEY (Clave) references materiales(Clave)
ALTER TABLE entregan add constraint FKRFC FOREIGN KEY (RFC) references proveedores(RFC)
ALTER TABLE entregan add constraint FKNumero FOREIGN KEY (Numero) references proyectos(Numero)
ALTER TABLE entregan add constraint cantidad check (cantidad > 0)
BULK INSERT a1208229.a1208229.[Materiales]
   FROM 'e:\wwwroot\a1208229\materiales.csv'
   WITH 
      (
         CODEPAGE = 'ACP',
         FIELDTERMINATOR = ',',
         ROWTERMINATOR = '\n'
      ) 
BULK INSERT a1208229.a1208229.[Proyectos]
   FROM 'e:\wwwroot\a1208229\proyectos.csv'
   WITH 
      (
         CODEPAGE = 'ACP',
         FIELDTERMINATOR = ',',
         ROWTERMINATOR = '\n'
      )
BULK INSERT a1208229.a1208229.[Proveedores]
   FROM 'e:\wwwroot\a1208229\proveedores.csv'
   WITH 
      (
         CODEPAGE = 'ACP',
         FIELDTERMINATOR = ',',
         ROWTERMINATOR = '\n'
      ) 
SET DATEFORMAT dmy
BULK INSERT a1208229.a1208229.[Entregan]
   FROM 'e:\wwwroot\a1208229\entregan.csv'
   WITH 
      (
         CODEPAGE = 'ACP',
         FIELDTERMINATOR = ',',
         ROWTERMINATOR = '\n'
      )