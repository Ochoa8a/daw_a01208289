<?php include("extra/_header.html"); ?>
        
        <section class="hero">
            <div class="wrapper">
                <header><h1>Laboratorio 11</h1></header>
                </div>
        </section>
        <section class="main">
            <div class="wrapper">
                <p>Usuario: Ochoa8a8</br>Contraseña: Al#jandr0</p>
                <div class="container">
                    <form action="login.php" method="post">
                        <label for="usrname">Usuario</label>
                        <input type="text" id="usrname" name="usrname" required>
                        <label for="psw">Contraseña</label>
                        <input type="password" id="psw" name="psw" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" title="Requiere 1 mayuscula, 1 minuscula, 1 numero y 8 caracteres" required>
                        <input type="submit" value="Enter" onclick="verify()">
                    </form>
                </div>
            </div>
            <h1>Preguntas:</h1>
				<ol>
					<li>¿Por qué es una buena práctica separar el controlador de la vista?<br/>-Porque es mas sencillo verlos y modificarlos por separado
					y de esta manera tener un código más organizado.</li>
					<li>Aparte de los arreglos $_POST y $_GET, ¿qué otros arreglos están predefinidos en php y cuál es su función?<br/>-#_COOKIE, 
					$_SESSION,$_REQUEST, entre otros. Sirven basicamente para pasar arrays de información de una página a otra.</li>
					<li>Explora las funciones de php, y describe 2 que no hayas visto en otro lenguaje y que llamen tu atención.<br/>-mail(): que te 
					permite mandar mails desde un script.<br/>-FTP: Te permite accesar a servidores a traves de FTP y hacer varias acciones.</li>
					<li>¿Qué es XSS y cómo se puede prevenir?<br/>-Cross side scripting, es una manera de inyectar scripts maliciosos a sitios, para 
					obtener información o causar algun daño. Se puede evitar usando htmlspecialchars() que asegura tu código y hace que todo lo que
					sea inyectado no se ejecute.</li>
				</ol>
        </section>
<?php include("extra/_footer.html"); ?>

        <script src="psw.js"></script>
    